Set-PSDebug -Trace 1

$YYYYQQ = (Get-Date -Format "yyyy") + "-Q" + [math]::Ceiling((Get-Date).Month / 3)

docker build --pull --no-cache --tag wireshark-windows-dev:$YYYYQQ .

docker tag wireshark-windows-dev:$YYYYQQ wireshark-windows-dev:latest

Set-PSDebug -Trace 0