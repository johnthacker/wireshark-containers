FROM debian:stable
ADD https://gitlab.com/wireshark/wireshark/-/raw/master/tools/debian-setup.sh /debian-setup-master.sh
ADD https://gitlab.com/wireshark/wireshark/-/raw/release-4.2/tools/debian-setup.sh /debian-setup-4.2.sh
ENV COMMON_APT_GET_ARGS="--yes --no-install-recommends"
ENV MINIO_MC_VERSION=2024-07-03T20-17-25Z
ENV MINIO_MC_SHA256=3a825efd7af6f455d63c732b923fa0db108585192db875797b71f1dc506644aa
RUN export DEBIAN_FRONTEND="noninteractive" DEBCONF_NONINTERACTIVE_SEEN=true \
	&& echo "force-unsafe-io" > /etc/dpkg/dpkg.cfg.d/unsafe-io \
	&& printf "tzdata tzdata/Areas select Etc\ntzdata tzdata/Zones/Etc select UTC" | debconf-set-selections \
	&& chmod -v +x debian-setup*.sh \
	&& apt-get update \
	&& apt-get install build-essential ${COMMON_APT_GET_ARGS} \
	&& ./debian-setup-4.2.sh ${COMMON_APT_GET_ARGS} --install-optional --install-qt5-deps \
	&& ./debian-setup-master.sh ${COMMON_APT_GET_ARGS} --install-optional --install-test-deps --install-deb-deps \
		curl \
		dpkg-dev \
		omniidl \
		snacc \
	&& curl --fail-with-body --location --output /usr/local/bin/mc https://dl.min.io/client/mc/release/linux-amd64/archive/mc.RELEASE.$MINIO_MC_VERSION \
	&& echo "$MINIO_MC_SHA256  /usr/local/bin/mc" | shasum --algorithm 256 --check \
	&& chmod u=rwx,go=rx /usr/local/bin/mc \
	&& dpkg-query -Wf '${Installed-Size}\t${Package}\n' | sort -rn | head -n 50 \
	&& rm -rf /var/lib/apt/lists/
