# Wireshark Fedora MinGW Development Image

Docker base image with various compilers and dependencies pre-installed 
to cross-compile Wireshark for Windows on Fedora.

This image is used by the “Windows MinGW-w64 package” job in Wireshark's GitLab Pipelines:
https://gitlab.com/wireshark/wireshark/pipelines.

You can use it via GitLab at
https://gitlab.com/wireshark/wireshark-containers/container_registry.

# Contributing

Please submit patches to
https://gitlab.com/wireshark/wireshark-containers.
See
https://gitlab.com/wireshark/wireshark-containers#contributing
for details.